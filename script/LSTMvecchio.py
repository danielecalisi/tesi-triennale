from datetime import datetime, timedelta
import time
import pytz
from collections import namedtuple
import pandas as pd
import requests
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from darksky import forecast
import statsmodels.api as sm
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_absolute_error, median_absolute_error, explained_variance_score
from keras.models import Sequential
from keras.layers import Dense, Activation
from keras.layers import LSTM, SimpleRNN
from keras.optimizers import SGD, Adam

import itertools
import time

def norm(x):
    return (x - train_stats['mean']) / train_stats['std']

def build_model():

    model = Sequential()
    model.add(LSTM(100, activation='tanh', return_sequences=True, input_shape=(1,len(X_train.keys()))))
    model.add(LSTM(100, activation='tanh'))
    model.add(Dense(1))
    model.add(Activation('linear'))

    opt = Adam(learning_rate=0.001, beta_1=0.9, beta_2=0.999, amsgrad=False)
    #opt = SGD(learning_rate=0.01, nesterov=True)

    model.compile(optimizer=opt,
                  loss='mse',
                  metrics=['mae', 'mse'])

    return model

if __name__ == "__main__":

    start_time = time.time()
    df = pd.read_csv('/home/daniele/Scrivania/newYork2GF.csv').set_index('dataora')

    parametro = 'temperature'

    featuresHourlyNoSummaryIcon = ["visibility", "temperature", "dewPoint", "humidity", "cloudCover",
            "apparentTemperature", "pressure", "windSpeed", "windGust", "windBearing", "uvIndex", "precipIntensity",
            "precipProbability"]

    daRimuovere = [feature for feature in featuresHourlyNoSummaryIcon if feature not in [parametro]]

    df = df.drop(columns=daRimuovere)

    # daRimuovere = [feature for feature in df.columns if feature not in ["pressure", "humidity_media1", "humidity_media2", "temperature_media1", "temperature_media2", "apparentTemperature_media1", "apparentTemperature_media2", "cloudCover_media1", "cloudCover_media2"]]
    # df = df.drop(columns=daRimuovere)


    X = df[[col for col in df.columns if col != parametro]]
    y = df[parametro]


    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.1, random_state=23)
    #il 10% verra usato per validation



    train_stats = X_train.describe().T
    normed_train_data = norm(X_train)
    normed_test_data = norm(X_test)



    normed_train_data = np.array(normed_train_data)
    normed_train_data = normed_train_data.reshape(len(normed_train_data), 1, len(X_train.keys()))

    normed_test_data = np.array(normed_test_data)
    normed_test_data = normed_test_data.reshape(len(normed_test_data), 1, len(X_train.keys()))


    model = build_model()


    EPOCHS = 300
    #early_stop = keras.callbacks.EarlyStopping(monitor='val_loss', patience=10)

    model.fit(normed_train_data, y_train, epochs=EPOCHS, validation_split = 0.1, shuffle=True, verbose=1) #callbacks=[early_stop])

    loss, mae, mse = model.evaluate(normed_test_data, y_test, verbose=2)


    print("Testing set Mean Abs Error: {:5.2f} ".format(mae))
    print("Testing set Mean Squared Error: {:5.2f} ".format(mse))

    predictions = model.predict(normed_test_data).flatten()

    tempo = (time.time() - start_time)

    a = plt.axes(aspect='equal')
    plt.scatter(y_test, predictions)
    plt.xlabel('True Values ')
    plt.ylabel('Predictions ')
    lims = [0, 1]    #980, 1050 per pressione
    plt.xlim(lims)
    plt.ylim(lims)
    _ = plt.plot(lims, lims)
    plt.show()

    predictionList = predictions.tolist()
    realList = y_test.tolist()
    count = 0
    for p,r in itertools.izip(predictionList,realList):
        err = abs(p - r)
        if err <= 1:                #DA CAMBIARE SE SI CAMBIA PARAMETRO
            count += 1
    percSucc = float(count) / len(predictionList)
    print(percSucc * 100)

    print("--- %s seconds ---" % (tempo))
