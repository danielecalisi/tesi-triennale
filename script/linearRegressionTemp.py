from datetime import datetime, timedelta
import time
import pytz
from collections import namedtuple
import pandas as pd
import requests
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from darksky import forecast
import statsmodels.api as sm

from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_absolute_error, median_absolute_error
import itertools

if __name__ == "__main__":

    df = pd.read_csv('/home/daniele/Scrivania/newYork2G.csv').set_index('dataora')
    
    #df.isna().sum()  #per vedere se ci sono buchi

    featuresHourlyNoSummaryIcon = ["visibility", "temperature", "dewPoint", "humidity", "cloudCover",
            "apparentTemperature", "pressure", "windSpeed", "windGust", "windBearing", "uvIndex", "precipIntensity",
            "precipProbability"]

    daRimuovere = [feature for feature in featuresHourlyNoSummaryIcon if feature not in ['temperature']]  #tutte quelle di quell ora originali tranne temperature (e dataora)

    df = df.drop(columns=daRimuovere)

    #print(df.corr()[['temperature']].sort_values('temperature'))
    #tengo solo quelli con valore assoluto di linearita maggiore di 0.6
    predictorsTemp = ["uvIndex_media2", "uvIndex_media1", "dewPoint_media2", "apparentTemperature_media2",
                    "temperature_media2", "dewPoint_media1", "temperature_media1", "apparentTemperature_media1"]

    df2 = df[['temperature'] + predictorsTemp]
    #rappresentazione grafica linearita

    # plt.rcParams['figure.figsize'] = [16, 22]
    # fig, axes = plt.subplots(nrows=4, ncols=2, sharey=True)
    # arr = np.array(predictorsTemp).reshape(4, 2)
    # for row, col_arr in enumerate(arr):
    #     for col, feature in enumerate(col_arr):
    #         axes[row, col].scatter(df2[feature], df2['temperature'])
    #         if col == 0:
    #             axes[row, col].set(xlabel=feature, ylabel='temperature')
    #         else:
    #             axes[row, col].set(xlabel=feature)
    # plt.show()

    X = df2[predictorsTemp]
    y = df2['temperature']

    X = sm.add_constant(X) #agg colonna "const" con tutti 1

    alpha = 0.05

    model = sm.OLS(y, X).fit()
    #print(model.summary())

    X = X.drop('const', axis=1)

    model = sm.OLS(y, X).fit()
    #print(model.summary())   #OK

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=12)

    regressor = LinearRegression()

    regressor.fit(X_train, y_train)

    prediction = regressor.predict(X_test)
    predictionList = prediction.tolist()
    realList = y_test.tolist() #1743 valori
    count = 0
    for p,r in itertools.izip(predictionList,realList):
        err = abs(p - r)
        if err < 1:
            count += 1
    percSucc = float(count) / 1743
    print(percSucc * 100)

    print("The Explained Variance: %.2f" % regressor.score(X_test, y_test))
    print("The Mean Absolute Error: %.2f degrees celsius" % mean_absolute_error(y_test, prediction))
    print("The Median Absolute Error: %.2f degrees celsius" % median_absolute_error(y_test, prediction))
