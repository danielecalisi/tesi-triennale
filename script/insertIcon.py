import pandas as pd
import itertools


if __name__ == "__main__":

    df = pd.read_csv('/home/daniele/Scrivania/newYork.csv').set_index('dataora')

    df['icon'] = df['icon'].astype('category')

    listaIcon = []
    listaIconPred = []

    # for i in df["icon"].tolist():
    #     if i == "clear-day":
    #         listaIcon.append(0)
    #     if i == "clear-night":
    #         listaIcon.append(0)
    #     if i == "rain":
    #         listaIcon.append(1)
    #     if i =="snow":
    #         listaIcon.append(2)
    #     if i =="sleet":
    #         listaIcon.append(2)
    #     if i =="wind":
    #         listaIcon.append(3)
    #     if i =="fog":
    #         listaIcon.append(4)
    #     if i =="cloudy":
    #         listaIcon.append(5)
    #     if i =="partly-cloudy-day":
    #         listaIcon.append(6)
    #     if i =="partly-cloudy-night":
    #         listaIcon.append(6)



    for i in df["icon"].tolist():
        if i == "clear-day":
            listaIcon.append("clear")
        if i == "clear-night":
            listaIcon.append("clear")
        if i == "rain":
            listaIcon.append("rain")
        if i =="snow":
            listaIcon.append("snow")
        if i =="sleet":
            listaIcon.append("snow")
        if i =="wind":
            listaIcon.append("wind")
        if i =="fog":
            listaIcon.append("fog")
        if i =="cloudy":
            listaIcon.append("cloudy")
        if i =="partly-cloudy-day":
            listaIcon.append("partly-cloudy")
        if i =="partly-cloudy-night":
            listaIcon.append("partly-cloudy")


    df["icon"] = listaIcon[:]

    dfPred = pd.read_csv('/home/daniele/Scrivania/predictions20.csv').set_index('dataora')

    for i in dfPred.index:
        listaIconPred.append(df["icon"][df.index == i].values[0])

    dfPred["icon"] = listaIconPred[:]

    dfPred.to_csv('/home/daniele/Scrivania/predictions20RF.csv', index=True)
