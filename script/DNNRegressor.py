from datetime import datetime, timedelta
import time
import pytz
from collections import namedtuple
import pandas as pd
import requests
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from darksky import forecast
import statsmodels.api as sm
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_absolute_error, median_absolute_error, explained_variance_score
import tensorflow as tf
import itertools

def wx_input_fn(X, y=None, num_epochs=None, shuffle=True, batch_size=400):
    return tf.estimator.inputs.pandas_input_fn(x=X,
                                               y=y,
                                               num_epochs=num_epochs,
                                               shuffle=shuffle,
                                               batch_size=batch_size)

if __name__ == "__main__":

    df = pd.read_csv('/home/daniele/Scrivania/newYork2G.csv').set_index('dataora')

    featuresHourlyNoSummaryIcon = ["visibility", "temperature", "dewPoint", "humidity", "cloudCover",
            "apparentTemperature", "pressure", "windSpeed", "windGust", "windBearing", "uvIndex", "precipIntensity",
            "precipProbability"]

    daRimuovere = [feature for feature in featuresHourlyNoSummaryIcon if feature not in ['temperature']]  #tutte quelle di quell ora originali tranne temperature (e dataora)

    df = df.drop(columns=daRimuovere)

    X = df[[col for col in df.columns if col != 'temperature']]
    y = df['temperature']

    X_train, X_tmp, y_train, y_tmp = train_test_split(X, y, test_size=0.2, random_state=23) #80% train

    X_test, X_val, y_test, y_val = train_test_split(X_tmp, y_tmp, test_size=0.5, random_state=23) #10% test e 10 val

    #train con 6969 ore, validation con 872, test con 871

    feature_cols = [tf.feature_column.numeric_column(col) for col in X.columns]

    regressor = tf.estimator.DNNRegressor(feature_columns=feature_cols,
                                      hidden_units=[50, 50])
    #circa 2285 epoche                          
    evaluations = []
    STEPS = 400
    for i in range(100):
        regressor.train(input_fn=wx_input_fn(X_train, y=y_train), steps=STEPS)
        evaluations.append(regressor.evaluate(input_fn=wx_input_fn(X_val,
                                                                   y_val,
                                                                   num_epochs=1,
                                                                   shuffle=False)))

    plt.rcParams['figure.figsize'] = [14, 10]

    loss_values = [ev['loss'] for ev in evaluations]
    training_steps = [ev['global_step'] for ev in evaluations]

    plt.scatter(x=training_steps, y=loss_values)
    plt.xlabel('Training steps ')
    plt.ylabel('Loss (SSE)')
    plt.show()

    pred = regressor.predict(input_fn=wx_input_fn(X_test,
                                              num_epochs=1,
                                              shuffle=False))
    predictions = np.array([p['predictions'][0] for p in pred])
    predictionList = predictions.tolist()
    realList = y_test.tolist()
    count = 0
    for p,r in itertools.izip(predictionList,realList):
        err = abs(p - r)
        if err < 1:
            count += 1
    percSucc = float(count) / (len(realList))
    print(percSucc * 100)
    print("The Explained Variance: %.2f" % explained_variance_score(
                                                y_test, predictions))
    print("The Mean Absolute Error: %.2f degrees Celcius" % mean_absolute_error(
                                                y_test, predictions))
    print("The Median Absolute Error: %.2f degrees Celcius" % median_absolute_error(
                                                y_test, predictions))
