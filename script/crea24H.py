from datetime import datetime, timedelta
import time
import pytz
from collections import namedtuple
import pandas as pd
import requests
import matplotlib.pyplot as plt
from darksky import forecast

def derive_nth_day_feature(df, feature, N):
    rows = df.shape[0]
    nth_prior_measurements = [None]*N + [df[feature][i-N] for i in range(N, rows)] #list comprehensions
    col_name = "{}_{}".format(feature, N)
    df[col_name] = nth_prior_measurements

if __name__ == "__main__":

    df = pd.read_csv('/home/daniele/Scrivania/newYork.csv').set_index('dataora')

    featuresHourlyNoSummaryIcon = ["dataora", "visibility", "temperature", "dewPoint", "humidity", "cloudCover",
            "apparentTemperature", "pressure", "windSpeed", "windGust", "windBearing", "uvIndex", "precipIntensity",
            "precipProbability"]

    df = df.drop(columns=['summary','icon'])

    for feature in featuresHourlyNoSummaryIcon:
        if feature != "dataora":
            for N in range(25, 49):
                derive_nth_day_feature(df, feature, N)

    df = df.dropna()

    df.to_csv('/home/daniele/Scrivania/newYork24.csv', index=True)
