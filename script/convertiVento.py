from datetime import datetime, timedelta
import time
import pytz
from collections import namedtuple
import pandas as pd
import requests
import matplotlib.pyplot as plt
import itertools
import math

if __name__ == "__main__":

    df = pd.read_csv('/home/daniele/Scrivania/newYork2GFH.csv').set_index('dataora')

    windSpeed = df['windSpeed'].tolist()
    windBearing = df['windBearing'].tolist()
    windSpeed_media1 = df['windSpeed_media1'].tolist()
    windBearing_media1 = df['windBearing_media1'].tolist()
    windSpeed_media2 = df['windSpeed_media2'].tolist()
    windBearing_media2 = df['windBearing_media2'].tolist()

    listaX = []
    listaY = []
    listaX1 = []
    listaY1 = []
    listaX2= []
    listaY2 = []
    radianti = 0



    for s,b,s1,b1,s2,b2 in itertools.izip(windSpeed,windBearing,windSpeed_media1,windBearing_media1,windSpeed_media2,windBearing_media2):
        radianti = math.radians(b)
        asseX = s * (math.sin(radianti))
        asseY = s * (math.cos(radianti))
        listaX.append(-asseX)  #gli cambio segno perche dall api indica da dove ARRIVA il vento. io voglio dove VA
        listaY.append(-asseY)

        radianti = math.radians(b1)
        asseX = s1 * (math.sin(radianti))
        asseY = s1 * (math.cos(radianti))
        listaX1.append(-asseX)  #gli cambio segno perche dall api indica da dove ARRIVA il vento. io voglio dove VA
        listaY1.append(-asseY)

        radianti = math.radians(b2)
        asseX = s2 * (math.sin(radianti))
        asseY = s2 * (math.cos(radianti))
        listaX2.append(-asseX)  #gli cambio segno perche dall api indica da dove ARRIVA il vento. io voglio dove VA
        listaY2.append(-asseY)

    df['windX'] = listaX[:]
    df['windY'] = listaY[:]
    df['windX_media1'] = listaX1[:]
    df['windY_media1'] = listaY1[:]
    df['windX_media2'] = listaX2[:]
    df['windY_media2'] = listaY2[:]

    daRimuovere = ["windSpeed","windBearing","windSpeed_media1","windBearing_media1","windSpeed_media2","windBearing_media2"]
    df = df.drop(columns=daRimuovere)

    df.to_csv('/home/daniele/Scrivania/newYork2GFHW.csv', index=True)
