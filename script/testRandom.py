from datetime import datetime, timedelta
import time
import pytz
from collections import namedtuple
import pandas as pd
import requests
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from darksky import forecast
import statsmodels.api as sm
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_absolute_error, median_absolute_error, explained_variance_score
from keras.models import Sequential
from keras.layers import Dense, Activation
from keras.layers import LSTM, SimpleRNN
from keras.optimizers import SGD, Adam

import itertools
import time

def norm(x):
    return (x - train_stats['mean']) / train_stats['std']

def build_model():

    model = Sequential()
    model.add(LSTM(100, activation='tanh', return_sequences=True, input_shape=(2,len(X_train.keys())/2)))
    model.add(LSTM(100, activation='tanh'))
    model.add(Dense(1))
    model.add(Activation('linear'))

    opt = Adam(learning_rate=0.001, beta_1=0.9, beta_2=0.999, amsgrad=False)
    #opt = SGD(learning_rate=0.01, nesterov=True)

    model.compile(optimizer=opt,
                  loss='mse',
                  metrics=['mae', 'mse'])

    return model

if __name__ == "__main__":


    df = pd.read_csv('/home/daniele/Scrivania/newYork2GF.csv').set_index('dataora')

    parametro = 'temperature'

    featuresHourlyNoSummaryIcon = ["visibility", "temperature", "dewPoint", "humidity", "cloudCover",
            "apparentTemperature", "pressure", "windSpeed", "windGust", "windBearing", "uvIndex", "precipIntensity",
            "precipProbability"]

    daRimuovere = [feature for feature in featuresHourlyNoSummaryIcon if feature not in [parametro]]

    df = df.drop(columns=daRimuovere)

    # daRimuovere = [feature for feature in df.columns if feature not in ["pressure", "humidity_media1", "humidity_media2", "temperature_media1", "temperature_media2", "apparentTemperature_media1", "apparentTemperature_media2", "cloudCover_media1", "cloudCover_media2"]]
    # df = df.drop(columns=daRimuovere)


    X = df[[col for col in df.columns if col != parametro]]
    y = df[parametro]


    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.1, random_state=23)
    #il 10% verra usato per validation





    df1 = pd.read_csv('/home/daniele/Scrivania/newYork2GFW.csv').set_index('dataora')

    parametro = 'pressure'



    features = ["visibility", "temperature", "dewPoint", "humidity", "cloudCover",
            "apparentTemperature", "pressure", "windX", "windGust", "windY", "uvIndex", "precipIntensity",
            "precipProbability"]

    daRimuovere = [feature for feature in features if feature not in [parametro]]

    df1 = df1.drop(columns=daRimuovere)

    # daRimuovere = [feature for feature in df.columns if feature not in ["pressure", "humidity_media1", "humidity_media2", "temperature_media1", "temperature_media2", "apparentTemperature_media1", "apparentTemperature_media2", "cloudCover_media1", "cloudCover_media2"]]
    # df = df.drop(columns=daRimuovere)


    X1 = df1[[col for col in df1.columns if col != parametro]]
    y1 = df1[parametro]


    X_train1, X_test1, y_train1, y_test1 = train_test_split(X1, y1, test_size=0.1, random_state=23)
    #il 10% verra usato per validation

    print(X_train1.index)
    # for x,x1 in itertools.izip(X_test["temperature_media1"].values.tolist(),X_test1["temperature_media1"].values.tolist()):
    #     print(x,x1)
