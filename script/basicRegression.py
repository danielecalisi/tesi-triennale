from __future__ import absolute_import, division, print_function, unicode_literals

import pathlib

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns

import tensorflow as tf

from tensorflow import keras
from tensorflow.keras import layers

#import tensorflow_docs as tfdocs
# import tensorflow_docs.plots
# import tensorflow_docs.modeling

def norm(x):
    return (x - train_stats['mean']) / train_stats['std']

def build_model():
    model = keras.Sequential([
    layers.Dense(64, activation='relu', input_shape=[len(train_dataset.keys())]),
    layers.Dense(64, activation='relu'),
    layers.Dense(1)
    ])

    optimizer = tf.keras.optimizers.RMSprop(0.001)

    model.compile(loss='mse',
                optimizer=optimizer,
                metrics=['mae', 'mse'])
    return model

if __name__ == "__main__":

    df1 = pd.read_csv('/home/daniele/Scrivania/newYork2G.csv').set_index('dataora')

    featuresHourlyNoSummaryIcon = ["visibility", "temperature", "dewPoint", "humidity", "cloudCover",
            "apparentTemperature", "pressure", "windSpeed", "windGust", "windBearing", "uvIndex", "precipIntensity",
            "precipProbability"]

    dataset = df1.head(400)

    daRimuovere = [feature for feature in dataset.columns if feature not in featuresHourlyNoSummaryIcon]  #tolgo tutte quelle su giorni precedenti

    dataset = dataset.drop(columns=daRimuovere)

    column_names = [col for col in dataset.columns]

    train_dataset = dataset.sample(frac=0.8,random_state=0)
    test_dataset = dataset.drop(train_dataset.index)



    # sns.pairplot(train_dataset[["temperature", "windSpeed", "pressure"]], diag_kind="kde")
    # plt.show()

    train_stats = train_dataset.describe()
    train_stats.pop("temperature")
    train_stats = train_stats.transpose()

    train_labels = train_dataset.pop('temperature')
    test_labels = test_dataset.pop('temperature')

    normed_train_data = norm(train_dataset)
    normed_test_data = norm(test_dataset)

    model = build_model()

    #model.summary()

    EPOCHS = 1000

    early_stop = keras.callbacks.EarlyStopping(monitor='val_loss', patience=10)

    early_history = model.fit(normed_train_data, train_labels,
                    epochs=EPOCHS, validation_split = 0.2, verbose=0,
                    callbacks=[early_stop])

    loss, mae, mse = model.evaluate(normed_test_data, test_labels, verbose=2)

    test_predictions = model.predict(normed_test_data).flatten()

    # a = plt.axes(aspect='equal')
    # plt.scatter(test_labels, test_predictions)
    # plt.xlabel('True Values [temperature]')
    # plt.ylabel('Predictions [temperature]')
    # lims = [0, 50]
    # plt.xlim(lims)
    # plt.ylim(lims)
    # _ = plt.plot(lims, lims)

    error = test_predictions - test_labels
    plt.hist(error, bins = 25)
    plt.xlabel("Prediction Error [temperature]")
    _ = plt.ylabel("Count")
    plt.show()
