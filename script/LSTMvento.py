from datetime import datetime, timedelta
import time
import pytz
from collections import namedtuple
import pandas as pd
import requests
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from darksky import forecast
import statsmodels.api as sm
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_absolute_error, median_absolute_error, explained_variance_score
from keras.models import Sequential
from keras.layers import Dense, Activation
from keras.layers import LSTM, SimpleRNN
from keras.optimizers import SGD, Adam

import itertools
import time

def norm(x):
    return (x - train_stats['mean']) / train_stats['std']

def build_model():

    model = Sequential()
    model.add(LSTM(100, activation='tanh', return_sequences=True, input_shape=(2,len(X_train.keys())/2), recurrent_dropout=0.2))
    model.add(LSTM(100, activation='tanh'))
    model.add(Dense(1))
    model.add(Activation('linear'))

    opt = Adam(learning_rate=0.001, beta_1=0.9, beta_2=0.999, amsgrad=False)


    model.compile(optimizer=opt,
                  loss='mse',
                  metrics=['mae', 'mse'])

    return model

if __name__ == "__main__":

    df = pd.read_csv('/home/daniele/Scrivania/newYork2GFW.csv').set_index('dataora')
    dfN = pd.read_csv('/home/daniele/Scrivania/newYorkNORD2GFW.csv').set_index('dataora')
    dfE = pd.read_csv('/home/daniele/Scrivania/newYorkEST2GFW.csv').set_index('dataora')
    dfO = pd.read_csv('/home/daniele/Scrivania/newYorkOVEST2GFW.csv').set_index('dataora')
    dfS = pd.read_csv('/home/daniele/Scrivania/newYorkSUD2GFW.csv').set_index('dataora')

    rows = df.shape[0] #uguale per tutti

    ventoNord = []
    ventoEst = []
    ventoOvest = []
    ventoSud = []

    for N in range(1, 3):
        col_name = "{}_media{}".format("windY", N)
        for row in range(rows):
            if dfN[col_name][row] < 0:
                ventoNord.append(-dfN[col_name][row])
            else:
                ventoNord.append(0)
        col_name = "{}_media{}".format("windNORD", N)
        df[col_name] = ventoNord[:]
        del ventoNord[:]

    for N in range(1, 3):
        col_name = "{}_media{}".format("windX", N)
        for row in range(rows):
            if dfE[col_name][row] < 0:
                ventoEst.append(-dfE[col_name][row])
            else:
                ventoEst.append(0)
        col_name = "{}_media{}".format("windEST", N)
        df[col_name] = ventoEst[:]
        del ventoEst[:]

    for N in range(1, 3):
        col_name = "{}_media{}".format("windX", N)
        for row in range(rows):
            if dfO[col_name][row] > 0:
                ventoOvest.append(dfO[col_name][row])
            else:
                ventoOvest.append(0)
        col_name = "{}_media{}".format("windOVEST", N)
        df[col_name] = ventoOvest[:]
        del ventoOvest[:]

    for N in range(1, 3):
        col_name = "{}_media{}".format("windY", N)
        for row in range(rows):
            if dfS[col_name][row] > 0:
                ventoSud.append(dfS[col_name][row])
            else:
                ventoSud.append(0)
        col_name = "{}_media{}".format("windSUD", N)
        df[col_name] = ventoSud[:]
        del ventoSud[:]

    # df["pressureNORD_media1"] = dfN["pressure_media1"].tolist()
    # df["pressureNORD_media2"] = dfN["pressure_media2"].tolist()
    #
    # df["pressureEST_media1"] = dfE["pressure_media1"].tolist()
    # df["pressureEST_media2"] = dfE["pressure_media2"].tolist()
    #
    # df["pressureOVEST_media1"] = dfO["pressure_media1"].tolist()
    # df["pressureOVEST_media2"] = dfO["pressure_media2"].tolist()
    #
    # df["pressureSUD_media1"] = dfS["pressure_media1"].tolist()
    # df["pressureSUD_media2"] = dfS["pressure_media2"].tolist()




    df["cloudCoverNORD_media1"] = dfN["cloudCover_media1"].tolist()
    df["cloudCoverNORD_media2"] = dfN["cloudCover_media2"].tolist()

    df["cloudCoverEST_media1"] = dfE["cloudCover_media1"].tolist()
    df["cloudCoverEST_media2"] = dfE["cloudCover_media2"].tolist()

    df["cloudCoverOVEST_media1"] = dfO["cloudCover_media1"].tolist()
    df["cloudCoverOVEST_media2"] = dfO["cloudCover_media2"].tolist()

    df["cloudCoverSUD_media1"] = dfS["cloudCover_media1"].tolist()
    df["cloudCoverSUD_media2"] = dfS["cloudCover_media2"].tolist()

    #df.info()
    start_time = time.time()

    parametro = 'precipIntensity'

    features = ["visibility", "temperature", "dewPoint", "humidity", "cloudCover",
            "apparentTemperature", "pressure", "windX", "windGust", "windY", "uvIndex", "precipIntensity",
            "precipProbability"]

    daRimuovere = [feature for feature in features if feature not in [parametro]]

    df = df.drop(columns=daRimuovere)

    daRimuovere = ["windGust_media1","windGust_media2"]
    df = df.drop(columns=daRimuovere)



    X = df[[col for col in df.columns if col != parametro]]
    y = df[parametro]


    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=23)


    train_stats = X_train.describe().T
    normed_train_data = norm(X_train)
    normed_test_data = norm(X_test)


    normed_train_data = np.array(normed_train_data)
    normed_train_data = normed_train_data.reshape(len(normed_train_data), 2, len(X_train.keys())/2)

    normed_test_data = np.array(normed_test_data)
    normed_test_data = normed_test_data.reshape(len(normed_test_data), 2, len(X_train.keys())/2)


    model = build_model()


    EPOCHS = 300
    #early_stop = keras.callbacks.EarlyStopping(monitor='val_loss', patience=10)

    model.fit(normed_train_data, y_train, epochs=EPOCHS, validation_split = 0.1, shuffle=True, verbose=1) #callbacks=[early_stop])

    loss, mae, mse = model.evaluate(normed_test_data, y_test, verbose=2)


    print("Testing set Mean Abs Error: {:5.2f} ".format(mae))
    print("Testing set Mean Squared Error: {:5.2f} ".format(mse))

    predictions = model.predict(normed_test_data).flatten()

    tempo = (time.time() - start_time)

    a = plt.axes(aspect='equal')
    plt.scatter(y_test, predictions)
    plt.xlabel('True Values ')
    plt.ylabel('Predictions ')
    lims = [0, 1]    #980, 1050 per pressione
    plt.xlim(lims)
    plt.ylim(lims)
    _ = plt.plot(lims, lims)
    plt.show()

    predictionList = predictions.tolist()
    realList = y_test.tolist()
    count = 0
    for p,r in itertools.izip(predictionList,realList):
        err = abs(p - r)
        if err <= 0.15:                #DA CAMBIARE SE SI CAMBIA PARAMETRO
            count += 1
    percSucc = float(count) / len(predictionList)
    print(percSucc * 100)

    print("--- %s seconds ---" % (tempo))


    dfPred = pd.read_csv('/home/daniele/Scrivania/predictions20.csv').set_index('dataora')

    dfPred["predPrecipIntensity"] = predictionList[:]

    dfPred.to_csv('/home/daniele/Scrivania/predictions20.csv', index=True)
