import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import classification_report

if __name__ == "__main__":

    df = pd.read_csv('/home/daniele/Scrivania/predictionsRF.csv').set_index('dataora')

    factor = pd.factorize(df['icon'])
    df.icon = factor[0]
    definitions = factor[1]

    # print(df.icon.head(10))
    # print(definitions)

    X = df[[col for col in df.columns if col != "icon"]]
    y = df["icon"]

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=23)


    scaler = StandardScaler()
    X_train = scaler.fit_transform(X_train)
    X_test = scaler.transform(X_test)

    classifier = RandomForestClassifier(n_estimators = 25, criterion = 'gini', random_state = 40)

    classifier.fit(X_train, y_train)


    y_pred = classifier.predict(X_test)

    reversefactor = dict(zip(range(7),definitions))
    y_test = np.vectorize(reversefactor.get)(y_test)
    y_pred = np.vectorize(reversefactor.get)(y_pred)

    print(pd.crosstab(y_test, y_pred, rownames=['Classi'], colnames=['         Predette']))

    print(classification_report(y_test, y_pred))
