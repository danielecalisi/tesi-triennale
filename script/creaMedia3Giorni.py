from datetime import datetime, timedelta
import time
import pytz
from collections import namedtuple
import pandas as pd
import requests
import matplotlib.pyplot as plt
from darksky import forecast

def derive_nth_day_feature(df, feature, N):
    rows = df.shape[0]
    nth_prior_measurements = [None]*N + [df[feature][i-N] for i in range(N, rows)] #list comprehensions
    col_name = "{}_{}".format(feature, N)
    df[col_name] = nth_prior_measurements

if __name__ == "__main__":

    df = pd.read_csv('/home/daniele/Scrivania/newYork.csv').set_index('dataora')

    featuresHourlyNoSummaryIcon = ["dataora", "visibility", "temperature", "dewPoint", "humidity", "cloudCover",
            "apparentTemperature", "pressure", "windSpeed", "windGust", "windBearing", "uvIndex", "precipIntensity",
            "precipProbability"]

    df = df.drop(columns=['summary','icon'])

    for feature in featuresHourlyNoSummaryIcon:
        if feature != "dataora":
            for N in range(1, 73):
                derive_nth_day_feature(df, feature, N)

    df = df.dropna()
    rows = df.shape[0]
    listaMedia1 = []
    listaMedia2 = []
    listaMedia3 = []
    for feature in featuresHourlyNoSummaryIcon:
        if feature != "dataora":
            for row in range(rows):
                media = 0
                for N in range(1, 25):
                    col_name = "{}_{}".format(feature, N)
                    media = df[col_name][row] + media
                listaMedia1.append(media)
                media = 0
                for N in range(25, 49):
                    col_name = "{}_{}".format(feature, N)
                    media = df[col_name][row] + media
                listaMedia2.append(media)
                media = 0
                for N in range(49, 73):
                    col_name = "{}_{}".format(feature, N)
                    media = df[col_name][row] + media
                listaMedia3.append(media)
            col_name = "{}_{}".format(feature, 'media1')
            df[col_name] = listaMedia1[:]
            col_name = "{}_{}".format(feature, 'media2')
            df[col_name] = listaMedia2[:]
            col_name = "{}_{}".format(feature, 'media3')
            df[col_name] = listaMedia3[:]
            del listaMedia1[:]
            del listaMedia2[:]
            del listaMedia3[:]
            for N in range(1, 73):
                col_name = "{}_{}".format(feature, N)
                df = df.drop(columns=[col_name])


    #daRimuovere = [feature for feature in featuresHourlyNoSummary if feature not in ['temperature']]  #tutte quelle di quell ora originali tranne temperature
    df.to_csv('/home/daniele/Scrivania/newYork3G.csv', index=True)
