from datetime import datetime, timedelta
import time
import pytz
from collections import namedtuple
import pandas as pd
import requests
import matplotlib.pyplot as plt
from darksky import forecast

def funzioneEstrazione(apiKey, dataTarget, timeZone, lat, long, days):
    records = []
    countVuote = 0
    countMaxVuoto = 0
    countMaxVuotoAppoggio = 0
    flagOraMancante = 0
    for n in range(days):
        print(n)
        dataTargetIso = dataTarget.isoformat()
        citta = forecast(apiKey, lat, long, time = dataTargetIso, units='si') #richiesta
        for k in range(24):
            try:
                dataoraUTC = datetime.utcfromtimestamp(citta['hourly']['data'][k]['time']).replace(tzinfo=pytz.utc)
                dataoraLOCAL = dataoraUTC.astimezone(timeZone)
                dataora = dataoraLOCAL.strftime('%Y-%m-%d %H:%M:%S')
            except IndexError:
                flagOraMancante = 1
            if (flagOraMancante == 1 or (len(citta['hourly']['data'][k]) < 16)):  #se l'ora manca opp l'ora non ha tutti i parametri

                countVuote += 1
                print("mancante")
                countMaxVuotoAppoggio += 1
                flagOraMancante = 0
                timeOraMancante = lastTime + (3600 * countMaxVuotoAppoggio)
                dataoraUTC = datetime.utcfromtimestamp(timeOraMancante).replace(tzinfo=pytz.utc)
                dataoraLOCAL = dataoraUTC.astimezone(timeZone)
                dataora = dataoraLOCAL.strftime('%Y-%m-%d %H:%M:%S')
                records.append(HourlySummary(
                dataora=dataora,
                visibility=lastVisibility,
                temperature=lastTemperature,
                icon=lastIcon,
                dewPoint=lastDewPoint,
                humidity=lastHumidity,
                cloudCover=lastCloudCover,
                summary=lastSummary,
                apparentTemperature=lastApparentTemperature,
                pressure=lastPressure,
                windSpeed=lastWindSpeed,
                windGust=lastWindGust,
                windBearing=lastWindBearing,
                uvIndex=lastUvIndex,
                precipIntensity=lastPrecipIntensity,
                precipProbability=lastPrecipProbability))

            else: #ora completa

                if countMaxVuotoAppoggio > countMaxVuoto:
                    countMaxVuoto = countMaxVuotoAppoggio
                countMaxVuotoAppoggio = 0
                records.append(HourlySummary(
                dataora=dataora,
                visibility=citta['hourly']['data'][k]['visibility'],
                temperature=citta['hourly']['data'][k]['temperature'],
                icon=citta['hourly']['data'][k]['icon'],
                dewPoint=citta['hourly']['data'][k]['dewPoint'],
                humidity=citta['hourly']['data'][k]['humidity'],
                cloudCover=citta['hourly']['data'][k]['cloudCover'],
                summary=citta['hourly']['data'][k]['summary'],
                apparentTemperature=citta['hourly']['data'][k]['apparentTemperature'],
                pressure=citta['hourly']['data'][k]['pressure'],
                windSpeed=citta['hourly']['data'][k]['windSpeed'],
                windGust=citta['hourly']['data'][k]['windGust'],
                windBearing=citta['hourly']['data'][k]['windBearing'],
                uvIndex=citta['hourly']['data'][k]['uvIndex'],
                precipIntensity=citta['hourly']['data'][k]['precipIntensity'],
                precipProbability=citta['hourly']['data'][k]['precipProbability']))

                lastTime=citta['hourly']['data'][k]['time']
                lastVisibility=citta['hourly']['data'][k]['visibility']
                lastTemperature=citta['hourly']['data'][k]['temperature']
                lastIcon=citta['hourly']['data'][k]['icon']
                lastDewPoint=citta['hourly']['data'][k]['dewPoint']
                lastHumidity=citta['hourly']['data'][k]['humidity']
                lastCloudCover=citta['hourly']['data'][k]['cloudCover']
                lastSummary=citta['hourly']['data'][k]['summary']
                lastApparentTemperature=citta['hourly']['data'][k]['apparentTemperature']
                lastPressure=citta['hourly']['data'][k]['pressure']
                lastWindSpeed=citta['hourly']['data'][k]['windSpeed']
                lastWindGust=citta['hourly']['data'][k]['windGust']
                lastWindBearing=citta['hourly']['data'][k]['windBearing']
                lastUvIndex=citta['hourly']['data'][k]['uvIndex']
                lastPrecipIntensity=citta['hourly']['data'][k]['precipIntensity']
                lastPrecipProbability=citta['hourly']['data'][k]['precipProbability']



        dataTarget += timedelta(days=1)
    if countMaxVuotoAppoggio > countMaxVuoto:
        countMaxVuoto = countMaxVuotoAppoggio
    print('num vuote: ' + str(countVuote))
    print('num max vuote contigue: ' + str(countMaxVuoto))
    return records

if __name__ == "__main__":

    API_KEY = '3009a40b6f14f16a3d73e6b8656afc33'
    data_target = datetime(2019, 1, 1, 0, 0, 0)  #anno mese giorno ora min sec
    tz = pytz.timezone('America/New_York')
    #new york 40.7142700 -74.0059700
    #50km verso est (republic airport) 40.719441 -73.408328
    #50km verso nord 41.170334, -74.007536
    #50km verso ovest 40.717659, -74.600375
    #50km verso sud 40.262064, -73.999867
    lat = 40.262064
    long = -73.999867
    featuresHourly = ["dataora", "visibility", "temperature", "icon", "dewPoint", "humidity", "cloudCover", "summary",
            "apparentTemperature", "pressure", "windSpeed", "windGust", "windBearing", "uvIndex", "precipIntensity",
            "precipProbability"]
    HourlySummary = namedtuple("HourlySummary", featuresHourly)
    records = []
    records = funzioneEstrazione(API_KEY, data_target, tz, lat, long, 365) #verranno fatte 365 richieste

    df = pd.DataFrame(records, columns=featuresHourly).set_index('dataora')
    df.to_csv('/home/daniele/Scrivania/newYorkSUD.csv', index=True)
