from datetime import datetime, timedelta
import time
import pytz
from collections import namedtuple
import pandas as pd
import requests
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from darksky import forecast
import statsmodels.api as sm
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_absolute_error, median_absolute_error, explained_variance_score
from sklearn.metrics import classification_report
from keras.models import Sequential
from keras.layers import Dense, Activation
from keras.layers import LSTM, SimpleRNN
from keras.optimizers import SGD, Adam
import itertools
import time
from keras.utils import to_categorical

def norm(x):
    return (x - train_stats['mean']) / train_stats['std']

def fromCategoricalToList(categoricalList):
    max = 0
    i = 0
    list = []

    for c in categoricalList:
        cont = 0
        max = 0
        for classe in c:
            if classe > max:
                max = classe
                i = cont
            cont += 1

        if i == 0:
            list.append("clear")
        if i == 1:
            list.append("rain")
        if i == 2:
            list.append("snow")
        if i == 3:
            list.append("wind")
        if i == 4:
            list.append("fog")
        if i == 5:
            list.append("cloudy")
        if i == 6:
            list.append("partly-cloudy")

    return list


def build_model():

    model = Sequential()
    model.add(Dense(350, activation='relu', input_shape=[len(X_train.keys())]))
    model.add(Dense(50, activation='relu'))
    model.add(Dense(num_categories, activation='softmax'))

    opt = Adam(learning_rate=0.001, beta_1=0.9, beta_2=0.999, amsgrad=False)
    #opt = SGD(learning_rate=0.01, nesterov=True)

    model.compile(loss='categorical_crossentropy',
                optimizer= opt,
                metrics=['accuracy'])

    return model

if __name__ == "__main__":

    start_time = time.time()
    df = pd.read_csv('/home/daniele/Scrivania/predictions20.csv').set_index('dataora')

    parametro = 'icon'   #obiettivo da classificare
    num_categories = 7

    # features = ["predTemperature", "predHumidity", "predCloudCover", "predPressure",
    # "predWindX", "predWindY", "predPrecipIntensity", "predPrecipProbability", "icon"]

    # daRimuovere = [feature for feature in df.columns if feature in ["predPrecipIntensity", "predPrecipProbability"]]
    # df = df.drop(columns=daRimuovere)

    X = df[[col for col in df.columns if col != parametro]]
    y = df[parametro]



    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=23)

    train_stats = X_train.describe().T
    normed_train_data = norm(X_train)
    normed_test_data = norm(X_test)


    y_train = to_categorical(y_train, num_categories)
    y_test = to_categorical(y_test, num_categories)


    model = build_model()
    #model.summary()

    EPOCHS = 50
    #early_stop = keras.callbacks.EarlyStopping(monitor='val_loss', patience=10)

    model.fit(normed_train_data, y_train, epochs=EPOCHS, validation_split = 0.1, verbose=1) #callbacks=[early_stop])

    test_results = model.evaluate(normed_test_data, y_test, verbose=1)
    #print(f'Test results - Loss: {test_results[0]} - Accuracy: {test_results[1]}%')

    print("Testing set Loss: {:5.2f} ".format(test_results[0]))
    print("Testing set Accuracy: {:5.2f} ".format(test_results[1]))


    predictions = model.predict(normed_test_data)

    tempo = (time.time() - start_time)

    predictionList = predictions.tolist()
    realList = y_test.tolist()

    predictionList = fromCategoricalToList(predictionList)
    realList = fromCategoricalToList(realList)

    predictionList = np.asarray(predictionList)
    realList = np.asarray(realList)

    print(pd.crosstab(realList, predictionList, rownames=['Classi reali'], colnames=['Classi predette']))

    print(classification_report(realList, predictionList))
